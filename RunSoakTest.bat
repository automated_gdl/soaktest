@ECHO OFF
PATH=%PATH%;
set start_date=%DATE%
echo "Scan results" > SoakResults

goto loop

:reset
taskkill /fi "WindowTitle eq Soak test*"
FOR /F %%x IN (Devices.txt) DO (
    mkdir "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\SoakTest\%DATE%\%%x"
    adb -s %%x shell mkdir sdcard/Automation/
    adb -s %%x push SoakTest.sh SoakResults /sdcard/Automation/
)
set start_date=%DATE%

:loop
FOR /F %%x IN (Devices.txt) DO (
    tasklist /FI "WINDOWTITLE eq Soak test %%x" | findstr /B "INFO:" > nul && start "Soak test %%x" /HIGH cmd /c "adb -s %%x shell sh /sdcard/Automation/SoakTest.sh"
    adb -s %%x pull /sdcard/Automation/SoakResults "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\SoakTest\%DATE%\%%x\SoakResults.csv"
    timeout /t 1 /nobreak > nul
)

if %start_date% NEQ %DATE% goto reset 
timeout /t 60 /nobreak > nul
goto loop

echo Press any key to exit...
pause >nul
exit