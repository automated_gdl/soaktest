#!/bin/bash
Gre='\e[0;32m'; On_Red='\e[41m'
RCol='\e[0m'; BYel='\e[1;33m';
cd sdcard/Automation
settings put secure sleep_timeout -1

stream1=0
stream2=0
freeze=0
start_time="$(date -u +%s)"

timestamp=$(date "+%Y-%m-%d %H:%M:%S") 
Snum=$(getprop ro.serialno)
MacA=$(getprop ro.boot.mac)
Build=$(getprop ro.product.version.base)
Launcher=$(dumpsys package com.dotscreen.megacable.livetv | grep versionName | cut -d "=" -f2)

#Prints device under test information.

echo "Serial Number","$Snum" >> SoakResults
echo "MAC Address","$MacA" >> SoakResults
echo "Build","${Build:0:6}" >> SoakResults
echo "Launcher","${Launcher:0:6}" >> SoakResults
echo "#Freezes,Freeze Timestamps" >> SoakResults

run_profiles() {
  Profiles=$(dumpsys window windows | grep -E mCurrentFocus)
  if [[ $Profiles == *"ProfileManagementActivity"* ]]; then
    sleep 1
    input keyevent KEYCODE_BACK      #Closes PIN menu if opened
    input keyevent KEYCODE_DPAD_DOWN #LOCATES SELECTION TO MAIN PROFILE
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_CENTER
    sleep 1
    input text "0000"
  fi
  return
}

focus_app() {
  fApp=$(dumpsys window windows | grep -E 'mFocusedApp') #current app
  if [[ $fApp != *"com.dotscreen.megacable.livetv"* ]]; then
    echo "LiveTV was unexpectedly closed. Re-launching LiveTV..."
    input keyevent KEYCODE_HOME
    sleep 1
    input keyevent KEYCODE_HOME
    sleep 1
    input text "260"
    sleep 5
  fi
  return
}

run_profiles

input keyevent KEYCODE_HOME
sleep 2
input keyevent KEYCODE_HOME
input text "260"
sleep 10

  echo "----------------------"
  echo "Running Soak test v1.0"
  echo "----------------------"
  while : 
  do
      run_profiles
      focus_app
      #stream1=$(cat /sys/module/amvideo/parameters/display_frame_count)
      #sleep 600
      #stream2=$(cat /sys/module/amvideo/parameters/display_frame_count)
      #timestamp=$(date "+%Y-%m-%d %H:%M:%S")
      #if [[ $stream1 == $stream2 ]]; then
       # echo -e $On_Red '\e[0K\rVideo is freeze!'$RCol
        #freeze=$(($freeze+1))
        #echo "$freeze,$timestamp" >> SoakResults
        #sleep 5
        #input keyevent KEYCODE_CHANNEL_UP 
        #sleep 5
        #input keyevent KEYCODE_CHANNEL_DOWN
        #Sleep 5
        #start_time="$(date -u +%s)"
      #else
       #curr_time="$(date -u +%s)"
       #MBF="$(($curr_time-$start_time))"
       #HrsMBF="$(printf '%02d:%02d:%02d\n' $(($MBF/3600)) $(($MBF%3600/60)) $(($MBF%60)))"
       #echo -n -e $Gre "\e[0K\rVideo playback OK! $BYel Video runtime:$RCol $HrsMBF $BYel Freeze counter:$RCol $freeze"$RCol
      #fi
   done
exit